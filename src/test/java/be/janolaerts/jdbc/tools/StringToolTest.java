package be.janolaerts.jdbc.tools;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class StringToolTest {
   static final LocalDateTime dateTime1 = LocalDateTime.of(
           1984, 9, 3, 12, 30, 21);
   static final LocalDateTime dateTime2 = LocalDateTime.of(
           2001, 9, 3, 12, 30, 21);
   static final LocalDateTime dateTime1NoSeconds = LocalDateTime.of(
           1984, 9, 3, 12, 30);
   static final LocalDateTime dateTime2NoSeconds = LocalDateTime.of(
           2001, 9, 3, 12, 30);
   static final String dateTime1Short = "3-9-84 12:30:21";
   static final String dateTime2Short = "3-9-01 12:30:21";
   static final String dateTime1Short_noSeconds = "3-9-84 12:30";
   static final String dateTime2Short_noSeconds = "3-9-01 12:30";
   static final String dateTime1Short_zeroSeconds = "3-9-84 12:30:00";
   static final String dateTime2Short_zeroSeconds = "3-9-01 12:30:00";
   static final String dateTime1MediumDashed = "03-09-1984 12:30:21";
   static final String dateTime2MediumDashed = "03-09-2001 12:30:21";
   static final String dateTime1MediumSpaced = "03 Sep 1984 12:30:21";
   static final String dateTime2MediumSpaced = "03 Sep 2001 12:30:21";
   static final String dateTime1MediumDashed_noSeconds = "03-09-1984 12:30";
   static final String dateTime2MediumDashed_noSeconds = "03-09-2001 12:30";
   static final String dateTime1MediumSpaced_noSeconds = "03 Sep 1984 12:30";
   static final String dateTime2MediumSpaced_noSeconds = "03 Sep 2001 12:30";
   static final String dateTime1MediumDashed_zeroSeconds = "03-09-1984 12:30:00";
   static final String dateTime2MediumDashed_zeroSeconds = "03-09-2001 12:30:00";
   static final String dateTime1MediumSpaced_zeroSeconds = "03 Sep 1984 12:30:00";
   static final String dateTime2MediumSpaced_zeroSeconds = "03 Sep 2001 12:30:00";
   static final String dateTime1Long = "03 September 1984 12:30:21";
   static final String dateTime2Long = "03 September 2001 12:30:21";

   @Test
   void dateTimeToMetricShortString() {
      assertEquals(dateTime1Short, StringTool.dateTimeToMetricShortString(dateTime1));
      assertEquals(dateTime2Short, StringTool.dateTimeToMetricShortString(dateTime2));
      assertEquals(dateTime1Short_zeroSeconds, StringTool.dateTimeToMetricShortString(dateTime1NoSeconds));
      assertEquals(dateTime2Short_zeroSeconds, StringTool.dateTimeToMetricShortString(dateTime2NoSeconds));
      assertEquals(dateTime1Short_noSeconds, StringTool.dateTimeToMetricShortStringNoSeconds(dateTime1));
      assertEquals(dateTime2Short_noSeconds, StringTool.dateTimeToMetricShortStringNoSeconds(dateTime2));
      assertEquals(dateTime1Short_noSeconds, StringTool.dateTimeToMetricShortStringNoSeconds(dateTime1NoSeconds));
      assertEquals(dateTime2Short_noSeconds, StringTool.dateTimeToMetricShortStringNoSeconds(dateTime2NoSeconds));
   }

   @Test
   void dateTimeToMetricMediumDashedString() {
      assertEquals(StringTool.DEFAULT_FAILED_DATETIME_RETURN_STRING, StringTool.dateTimeToMetricMediumDashedString(null));
      assertEquals(dateTime1MediumDashed, StringTool.dateTimeToMetricMediumDashedString(dateTime1));
      assertEquals(dateTime2MediumDashed, StringTool.dateTimeToMetricMediumDashedString(dateTime2));
      assertEquals(dateTime1MediumDashed_zeroSeconds, StringTool.dateTimeToMetricMediumDashedString(dateTime1NoSeconds));
      assertEquals(dateTime2MediumDashed_zeroSeconds, StringTool.dateTimeToMetricMediumDashedString(dateTime2NoSeconds));
      assertEquals(dateTime1MediumDashed_noSeconds, StringTool.dateTimeToMetricMediumDashedStringNoSeconds(dateTime1));
      assertEquals(dateTime2MediumDashed_noSeconds, StringTool.dateTimeToMetricMediumDashedStringNoSeconds(dateTime2));
      assertEquals(dateTime1MediumDashed_noSeconds, StringTool.dateTimeToMetricMediumDashedStringNoSeconds(dateTime1NoSeconds));
      assertEquals(dateTime2MediumDashed_noSeconds, StringTool.dateTimeToMetricMediumDashedStringNoSeconds(dateTime2NoSeconds));
   }

   @Test
   void dateTimeToMetricMediumSpacedString() {
      assertEquals(StringTool.DEFAULT_FAILED_DATETIME_RETURN_STRING, StringTool.dateTimeToMetricMediumSpacedString(null));
      assertEquals(dateTime1MediumSpaced, StringTool.dateTimeToMetricMediumSpacedString(dateTime1));
      assertEquals(dateTime2MediumSpaced, StringTool.dateTimeToMetricMediumSpacedString(dateTime2));
      assertEquals(dateTime1MediumSpaced_zeroSeconds, StringTool.dateTimeToMetricMediumSpacedString(dateTime1NoSeconds));
      assertEquals(dateTime2MediumSpaced_zeroSeconds, StringTool.dateTimeToMetricMediumSpacedString(dateTime2NoSeconds));
      assertEquals(dateTime1MediumSpaced_noSeconds, StringTool.dateTimeToMetricMediumSpacedStringNoSeconds(dateTime1));
      assertEquals(dateTime2MediumSpaced_noSeconds, StringTool.dateTimeToMetricMediumSpacedStringNoSeconds(dateTime2));
      assertEquals(dateTime1MediumSpaced_noSeconds, StringTool.dateTimeToMetricMediumSpacedStringNoSeconds(dateTime1NoSeconds));
      assertEquals(dateTime2MediumSpaced_noSeconds, StringTool.dateTimeToMetricMediumSpacedStringNoSeconds(dateTime2NoSeconds));
   }

   @Test
   void dateTimeToMetricLongString() {
      assertEquals(StringTool.DEFAULT_FAILED_DATETIME_RETURN_STRING, StringTool.dateTimeToMetricLongString(null));
      assertEquals(dateTime1Long, StringTool.dateTimeToMetricLongString(dateTime1));
      assertEquals(dateTime2Long, StringTool.dateTimeToMetricLongString(dateTime2));
   }

   @Test
   void metricShortStringToDateTime() {
      assertEquals(null, StringTool.metricShortStringToDateTime(null));
      assertEquals(dateTime1.plusYears(100), StringTool.metricShortStringToDateTime(dateTime1Short));
      assertEquals(dateTime2, StringTool.metricShortStringToDateTime(dateTime2Short));
      assertEquals(dateTime1NoSeconds.plusYears(100), StringTool.metricShortStringToDateTime(dateTime1Short_noSeconds));
      assertEquals(dateTime2NoSeconds, StringTool.metricShortStringToDateTime(dateTime2Short_noSeconds));
   }

   @Test
   void metricMediumDashedStringToDateTime() {
      assertEquals(null, StringTool.metricMediumDashedStringToDateTime(null));
      assertEquals(dateTime1, StringTool.metricMediumDashedStringToDateTime(dateTime1MediumDashed));
      assertEquals(dateTime2, StringTool.metricMediumDashedStringToDateTime(dateTime2MediumDashed));
      assertEquals(dateTime1NoSeconds, StringTool.metricMediumDashedStringToDateTime(dateTime1MediumDashed_noSeconds));
      assertEquals(dateTime2NoSeconds, StringTool.metricMediumDashedStringToDateTime(dateTime2MediumDashed_noSeconds));
   }

   @Test
   void metricMediumSpacedStringToDateTime() {
      assertEquals(null, StringTool.metricMediumSpacedStringToDateTime(null));
      assertEquals(dateTime1, StringTool.metricMediumSpacedStringToDateTime(dateTime1MediumSpaced));
      assertEquals(dateTime2, StringTool.metricMediumSpacedStringToDateTime(dateTime2MediumSpaced));
      assertEquals(dateTime1NoSeconds, StringTool.metricMediumSpacedStringToDateTime(dateTime1MediumSpaced_noSeconds));
      assertEquals(dateTime2NoSeconds, StringTool.metricMediumSpacedStringToDateTime(dateTime2MediumSpaced_noSeconds));
   }

   @Test
   void metricLongStringToDateTime() {
      assertEquals(null, StringTool.metricLongStringToDateTime(null));
      assertEquals(dateTime1, StringTool.metricLongStringToDateTime(dateTime1Long));
      assertEquals(dateTime2, StringTool.metricLongStringToDateTime(dateTime2Long));
   }
}