package be.janolaerts.jdbc.opdracht8;

import java.sql.*;

public class UpdateStock {

    private static final String URL = "jdbc:mariadb://javadev-training.be/javadevt_Hever7";
    private static final String USERNAME = "javadevt_StudHe";
    private static final String PASSWORD = "STUDENTvj2020";

    public static void main(String[] args) {
        updateStock();
    }

    public static void updateStock() {

        String cmd = "SELECT * FROM Beers;";

        try (Connection con = DriverManager.getConnection(URL, USERNAME, PASSWORD)) {

            try (Statement stmt = con.createStatement(
                 ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
                 ResultSet rs = stmt.executeQuery(cmd);) {

                 while (rs.next()) {
                     int stock = rs.getInt("Stock");
                     rs.updateInt("Stock", stock - 50);
                     rs.updateRow();
                    }

            } catch (SQLException se) {
                System.out.println(se.getMessage());
            }

        } catch(SQLException se) {
            System.out.println(se.getMessage());
        }
    }
}