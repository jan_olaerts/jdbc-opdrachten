package be.janolaerts.jdbc.tools;

/**
 * ConsolePrintTool
 * For special console printing needs
 *
 * @version 2.0.1 , 10-may-2020
 * */
public final class ConsolePrintTool {
   private ConsolePrintTool(){}

   public static void printTitle(String title, char type){
      StringBuilder sb = new StringBuilder();
      for(int i = 0; i< title.length(); i++){
         sb.append(type);
      }
      System.out.println(title);
      System.out.println(sb.toString());
   }

   public static void printTitle(String title){
      printTitle(title, '-');
   }
}
