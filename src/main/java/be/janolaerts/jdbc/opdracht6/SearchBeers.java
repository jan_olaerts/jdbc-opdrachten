package be.janolaerts.jdbc.opdracht6;

import java.sql.*;

public class SearchBeers {

    private static final String URL = "jdbc:mariadb://javadev-training.be/javadevt_Hever7";
    private static final String USERNAME = "javadevt_StudHe";
    private static final String PASSWORD = "STUDENTvj2020";

    public static void main(String[] args) {
        searchBeersWithDefinedAlcohol();
    }

    public static void searchBeersWithDefinedAlcohol() {

        String sql = "SELECT * FROM Beers WHERE Alcohol >= ? AND Alcohol <= ? ORDER BY Alcohol;";

        try (Connection con = DriverManager.getConnection(URL, USERNAME, PASSWORD)) {

            try (PreparedStatement stmt = con.prepareStatement(sql);) {
                stmt.setFloat(1, 2F);
                stmt.setFloat(2, 5F);
                ResultSet rs = stmt.executeQuery();

                while(rs.next()) {
                    String name = rs.getString("Name");
                    Float alcohol = rs.getFloat("Alcohol");

                    System.out.format("%-70s %.1f%n", name, alcohol);
                }
            }

        } catch(SQLException se) {
            System.out.println(se.getMessage());
        }
    }
}