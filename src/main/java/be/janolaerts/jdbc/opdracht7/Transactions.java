package be.janolaerts.jdbc.opdracht7;

import java.sql.*;

public class Transactions {

    private static final String URL = "jdbc:mariadb://javadev-training.be/javadevt_Hever7";
    private static final String USERNAME = "javadevt_StudHe";
    private static final String PASSWORD = "STUDENTvj2020";

    public static void main(String[] args) {
        adaptStock();
    }

    public static void adaptStock() {

        String sql1 = "UPDATE Beers SET Stock=? WHERE Name=?;";
        String sql2 = "UPDATE Beers SET Stock=? WHERE Name=?;";

        try (Connection con = DriverManager.getConnection(URL, USERNAME, PASSWORD)) {

            try (PreparedStatement stmt1 = con.prepareStatement(sql1);
                PreparedStatement stmt2 = con.prepareStatement(sql2)) {

                con.setAutoCommit(false);

                stmt1.setInt(1, 25);
                stmt1.setString(2, "Astor");

                stmt2.setInt(1, 35);
                stmt2.setString(2, "Barbar");

                stmt1.executeQuery();
                stmt2.executeQuery();

//                con.rollback();
                con.commit();

            } catch(SQLException se) {
                con.rollback();
                System.out.println(se);
            }
        } catch(SQLException se) {
            System.out.println(se);
        }
    }
}