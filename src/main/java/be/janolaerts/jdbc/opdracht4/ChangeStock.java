package be.janolaerts.jdbc.opdracht4;

import java.sql.*;

public class ChangeStock {

    private static final String URL = "jdbc:mariadb://javadev-training.be/javadevt_Hever7";
    private static final String USERNAME = "javadevt_StudHe";
    private static final String PASSWORD = "STUDENTvj2020";

    public static void main(String[] args) {
        changeStock();
    }

    public static void changeStock() {

        String sql = "UPDATE Beers SET Stock=40 WHERE Name LIKE '%Kriek%';";

        try (Connection con = DriverManager.getConnection(URL, USERNAME, PASSWORD)) {

            try (Statement stmt = con.createStatement()) {
                int result = stmt.executeUpdate(sql);
                System.out.println(result);

            } catch (SQLException se) {
                System.out.println(se.getMessage());
            }
        } catch (SQLException se) {
            System.out.println(se.getMessage());
        }
    }
}