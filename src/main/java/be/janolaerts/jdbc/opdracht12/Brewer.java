package be.janolaerts.jdbc.opdracht12;

public class Brewer {

    private int id;
    private String name;
    private String address;
    private String zipcode;
    private String city;
    private int turnover;

    public Brewer(int id, String name, String address, String zipcode, String city, int turnover) {
        if(id < 0) throw new BeerException("id cannot be negative");
        if(name == null) throw new BeerException("name cannot be null");
        if(address == null) throw new BeerException("address cannot bu null");
        if(zipcode == null || zipcode.matches("[a-zA-Z]")) throw new BeerException("zipcode cannot be null or cannot contain letters");
        if(city == null || city.matches("[d]")) throw new BeerException("city cannot be null or cannot contain digits");

        this.id = id;
        this.name = name;
        this.address = address;
        this.zipcode = zipcode;
        this.city = city;
        this.turnover = turnover;
    }

    public int getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public String getAddress() {
        return address;
    }
    public String getZipcode() {
        return zipcode;
    }
    public String getCity() {
        return city;
    }
    public int getTurnover() {
        return turnover;
    }

    @Override
    public String toString() {
        return String.format("%d %s %s %s %s %d", id, name, address, zipcode, city, turnover);
    }
}