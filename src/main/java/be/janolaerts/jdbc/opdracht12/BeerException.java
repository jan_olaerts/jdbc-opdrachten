package be.janolaerts.jdbc.opdracht12;

public class BeerException extends RuntimeException {

    public BeerException(String message) {
        super(message);
    }
}