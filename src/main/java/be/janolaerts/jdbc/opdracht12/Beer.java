package be.janolaerts.jdbc.opdracht12;

public class Beer {

    private int id;
    private String name;
    private int brewerId;
    private int categoryId;
    private float price;
    private int stock;
    private float alcohol;
    private Brewer brewer;

    public Beer(int id, String name, int brewerId, int categoryId, float price, int stock, float alcohol, Brewer brewer) {
        if(id < 0) throw new BeerException("Id cannot be negative");
        if(name == null) throw new BeerException("Name cannot be null");
        if(brewerId < 0) throw new BeerException("brewerId cannot be negative");
        if(categoryId < 0) throw new BeerException("categoryId cannot be negative");
        if(price < 0) throw new BeerException("Price cannot be negative");
        if(stock < 0) throw new BeerException("Stock cannot be negative");
        if(alcohol < 0) throw new BeerException("Alcohol cannot be negative");
        if(brewer == null) throw new BeerException("brewer cannot be null");

        this.id = id;
        this.name = name;
        this.brewerId = brewerId;
        this.categoryId = categoryId;
        this.price = price;
        this.stock = stock;
        this.alcohol = alcohol;
        this.brewer = brewer;
    }

    public int getId() {
        return id;
    }
    public int getBrewerId() {
        return brewerId;
    }
    public int getCategoryId() {
        return categoryId;
    }
    public String getName() {
        return name;
    }
    public float getPrice() {
        return price;
    }
    public int getStock() {
        return stock;
    }
    public float getAlcohol() {
        return alcohol;
    }
    public Brewer getBrewer() { return brewer; }

    @Override
    public String toString() {
        return String.format("%-20d %-75s %-10.2f %-10d %-10.1f %-10s", id, name, price, stock, alcohol, brewer.getName());
    }
}