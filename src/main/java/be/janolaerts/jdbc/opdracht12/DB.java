package be.janolaerts.jdbc.opdracht12;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DB {

    private String url = "jdbc:mariadb://javadev-training.be/javadevt_Hever7";
    private String user = "javadevt_StudHe";
    private String password = "STUDENTvj2020";

    public DB() {
    }

    public DB(String url, String user, String password) {
        this.url = url;
        this.user = user;
        this.password = password;
    }

    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(url, user, password);
    }
}