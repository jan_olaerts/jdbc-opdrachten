package be.janolaerts.jdbc.opdracht12;

public interface BeerDao {

    Beer getBeerById(int id) throws BeerException;
    void updateBeer(Beer beer) throws BeerException;
}