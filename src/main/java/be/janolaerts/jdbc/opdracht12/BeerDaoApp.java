package be.janolaerts.jdbc.opdracht12;

import java.util.List;

public class BeerDaoApp {

    public static void main(String[] args) {
        BeerDaoImpl beerDao = new BeerDaoImpl();

        Beer beer1 = beerDao.getBeerById(289);
//        System.out.println(beer1);

//        beerDao.updateBeer(new Beer(121, "Bergenbrau", 6, 42, 2.0f, 100, 2));

        List<Beer> beersByAlcohol = beerDao.getBeersByAlcohol(0.0f);
//        beersByAlcohol.forEach(System.out::println);

        List<Beer> beersByName = beerDao.getBeersByName("Kriek");
        beersByName.forEach(System.out::println);

        List<Brewer> brewersByBeerName = beerDao.getBrewersByBeerName("Kriek");
//        brewersByBeerName.forEach(System.out::println);
    }
}