package be.janolaerts.jdbc.opdracht12;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BeerDaoImpl implements BeerDao {

    private DB dataBase = new DB();

    @Override
    public Beer getBeerById(int id) throws BeerException {
        if(id < 0) throw new BeerException("Id cannot be negative");

        String sql = "SELECT * FROM Beers WHERE Id=?;";

        try (Connection con = dataBase.getConnection()) {

            try (PreparedStatement stmt = con.prepareStatement(sql)) {
                stmt.setInt(1, id);
                ResultSet rs = stmt.executeQuery();

                if(rs.first()) {
                    return getBeerData(rs);
                } else {
                    return null;
                }

            } catch (SQLException se) {
                System.err.println(se.getMessage());
                throw new BeerException(se.getMessage());
            }

        } catch (SQLException se) {
            System.err.println(se.getMessage());
            throw new BeerException(se.getMessage());
        }
    }

    @Override
    public void updateBeer(Beer beer) {
        if(beer == null) throw new BeerException("Beer cannot be null");

        String sql = "UPDATE Beers SET Name=?, Price=?, Stock=?, Alcohol=? WHERE Id=?;";

        try (Connection con = dataBase.getConnection()) {

            try (PreparedStatement stmt = con.prepareStatement(sql)) {
                stmt.setString(1, beer.getName());
                stmt.setFloat(2, beer.getPrice());
                stmt.setInt(3, beer.getStock());
                stmt.setFloat(4, beer.getAlcohol());
                stmt.setInt(5, beer.getId());
                stmt.executeUpdate();

            } catch (SQLException se) {
                System.err.println(se.getMessage());
                throw new BeerException(se.getMessage());
            }

        } catch (SQLException se) {
            System.err.println(se.getMessage());
            throw new BeerException(se.getMessage());
        }
    }

    public List<Beer> getBeersByAlcohol(float alcohol) {
        if(alcohol < 0) throw new BeerException("Alcohol cannot be negative");

        List<Beer> beers = new ArrayList<>();
        String sql = "SELECT * FROM Beers WHERE Alcohol=?;";

        try (Connection con = dataBase.getConnection()) {

            try (PreparedStatement stmt = con.prepareStatement(sql)) {
                stmt.setFloat(1, alcohol);
                ResultSet rs = stmt.executeQuery();

                while(rs.next()) {
                    beers.add(getBeerData(rs));
                }

                return beers;
            }

        } catch (SQLException se) {
            System.out.println(se);
            throw new BeerException(se.getMessage());
        }
    }

    public List<Beer> getBeersByName(String name) {
        if(name == null) throw new BeerException("name cannot be null");

        List<Beer> beers = new ArrayList<>();
        String sql = "SELECT * FROM Beers WHERE Name LIKE ?;";

        try (Connection con = dataBase.getConnection()) {

            try (PreparedStatement stmt = con.prepareStatement(sql)) {

                stmt.setString(1, "%" + name + "%");
                ResultSet rs = stmt.executeQuery();

                while(rs.next()) {
                    beers.add(getBeerData(rs));
                }

                return beers;

            } catch (SQLException se) {
                System.out.println(se.getMessage());
                throw new BeerException(se.getMessage());
            }

        } catch (SQLException se) {
            System.out.println(se.getMessage());
            throw new BeerException(se.getMessage());
        }
    }

    public List<Brewer> getBrewersByBeerName(String beerName) {

        List<Brewer> brewers = new ArrayList<>();
        String sql = "SELECT * FROM Brewers WHERE Id IN (SELECT BrewerId FROM Beers WHERE Name LIKE ?);";

        try (Connection con = dataBase.getConnection()) {

            try (PreparedStatement stmt1 = con.prepareStatement(sql)) {

                stmt1.setString(1, "%" + beerName + "%");
                ResultSet rs = stmt1.executeQuery();

                while(rs.next()) {
                    brewers.add(getBrewerData(rs));
                }

                return brewers;

            } catch (SQLException se) {
                System.out.println(se.getMessage());
                throw new BeerException(se.getMessage());
            }

        } catch (SQLException se) {
            System.out.println(se.getMessage());
            throw new BeerException(se.getMessage());
        }
    }

    public Brewer getBrewerById(int brewerId) {

        String sql = "SELECT * FROM Brewers WHERE Id=?;";

        try (Connection con = dataBase.getConnection()) {

            try (PreparedStatement stmt = con.prepareStatement(sql)) {

                stmt.setInt(1, brewerId);
                ResultSet rs = stmt.executeQuery();

                while(rs.first()) {
                    return getBrewerData(rs);
                }

                return null;

            } catch (SQLException se) {
                System.out.println(se.getMessage());
                throw new BeerException(se.getMessage());
            }

        } catch (SQLException se) {
            System.out.println(se.getMessage());
            throw new BeerException(se.getMessage());
        }
    }

    private Beer getBeerData(ResultSet rs) throws SQLException {
        if(rs == null) throw new BeerException("rs cannot be null");
        int id = rs.getInt("Id");
        String name = rs.getString("Name");
        int brewerId = rs.getInt("BrewerId");
        int categoryId = rs.getInt("CategoryId");
        float price = rs.getFloat("Price");
        int stock = rs.getInt("Stock");
        float alcohol = rs.getFloat("Alcohol");
        Brewer brewer = getBrewerById(brewerId);
        return new Beer(id, name, brewerId, categoryId, price, stock, alcohol, brewer);
    }

    private Brewer getBrewerData(ResultSet rs) throws SQLException {
        int id = rs.getInt("Id");
        String name = rs.getString("Name");
        String address = rs.getString("Address");
        String zipcode = rs.getString("ZipCode");
        String city = rs.getString("City");
        int turnover = rs.getInt("Turnover");
        return new Brewer(id, name, address, zipcode, city, turnover);
    }
}