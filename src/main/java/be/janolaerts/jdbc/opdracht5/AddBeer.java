package be.janolaerts.jdbc.opdracht5;

import java.sql.*;

public class AddBeer {

    private static final String URL = "jdbc:mariadb://javadev-training.be/javadevt_Hever7";
    private static final String USERNAME = "javadevt_StudHe";
    private static final String PASSWORD = "STUDENTvj2020";

    public static void main(String[] args) {
        addBeer();
    }

    public static void addBeer() {

        String sql = "INSERT INTO Beers (Name, BrewerId, CategoryId, Price, Stock, Alcohol)" +
                " VALUES ('BestBeer', 3, 4, 5.5, 20, 5.0);";

        try (Connection con = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            Statement stmt = con.createStatement();) {

            stmt.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
            try (ResultSet rs = stmt.getGeneratedKeys();) {

                if(rs.next()) {
                    int id = rs.getInt(1);
                    System.out.println(id);
                }

            } catch (SQLException se) {
                System.out.println(se.getMessage());
            }
        } catch (SQLException se) {
            System.out.println(se.getMessage());
        }
    }
}