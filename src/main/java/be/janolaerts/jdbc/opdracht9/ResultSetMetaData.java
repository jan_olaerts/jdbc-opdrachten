package be.janolaerts.jdbc.opdracht9;

import java.sql.*;

public class ResultSetMetaData {

    private static final String URL = "jdbc:mariadb://javadev-training.be/javadevt_Hever7";
    private static final String USERNAME = "javadevt_StudHe";
    private static final String PASSWORD = "STUDENTvj2020";

    public static void main(String[] args) {
        printMetaData();
    }

    public static void printMetaData() {

        String sql = "SELECT * FROM Beers;";
        String sql2 = "ALTER TABLE Beers ALTER COLUMN ? SET PRO_SQL_WIDTH ?;";

        try (Connection con = DriverManager.getConnection(URL, USERNAME, PASSWORD)) {

            try (Statement stmt = con.createStatement();
                 ResultSet rs = stmt.executeQuery(sql);) {

                java.sql.ResultSetMetaData rsmd = rs.getMetaData();

                int count = 1;
                while(rs.next()) {
                    String columnName = rsmd.getColumnName(count);
                    int columnType = rsmd.getColumnType(count);
                    if(columnType == 12) System.out.println(columnName);

                    int width = rsmd.getPrecision(count);
                    try (PreparedStatement stmt2 = con.prepareStatement(sql2)) {

                        stmt2.setInt(count, 1);
                        stmt2.setInt(width, 2);
                    }
                    count++;
                }
            }

        } catch (SQLException se) {
            System.out.println(se.getMessage());
        }
    }
}