package be.janolaerts.jdbc.opdracht1;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectDB {

    public static void main(String[] args) {
        try (Connection con = DriverManager.getConnection(
                "jdbc:mariadb://javadev-training.be/javadevt_Hever7","javadevt_StudHe","STUDENTvj2020")) {

            System.out.println("Connection OK");
        } catch (Exception ex) {
            System.out.println("Oops, something went wrong");
            ex.printStackTrace(System.err);
        }
    }
}