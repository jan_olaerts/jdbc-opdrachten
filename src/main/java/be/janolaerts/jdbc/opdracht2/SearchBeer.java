package be.janolaerts.jdbc.opdracht2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class SearchBeer {

    private static final String URL = "jdbc:mariadb://javadev-training.be/javadevt_Hever7";
    private static final String USERNAME = "javadevt_StudHe";
    private static final String PASSWORD = "STUDENTvj2020";

    public static void main(String[] args) {
        getBeersData();
    }

    public static void getBeersData() {

        String sql = "SELECT * FROM Beers WHERE Alcohol >= 7;";
        String sql2 = "SELECT * FROM Beers " +
                "INNER JOIN Brewers ON Beers.BrewerId = Brewers.Id " +
                "INNER JOIN Categories On Beers.CategoryId = Categories.Id;";

        try (Connection con = DriverManager.getConnection(URL,USERNAME,PASSWORD)) {

            try (Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(sql2);) {

                while(rs.next()) {
                    String beerName = rs.getString("Beers.Name");
                    String brewerName = rs.getString("Brewers.Name");
                    double alcohol = rs.getDouble("Alcohol");
                    double price = rs.getDouble("Price");
                    String category = rs.getString("Categories.Category");
                    System.out.format("%s %s %s %s %s%n", beerName, brewerName, alcohol, price, category);
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

        } catch (Exception ex) {
            System.out.println("Oops, something went wrong");
            ex.printStackTrace(System.err);
        }
    }
}